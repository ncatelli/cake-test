## This is for the alternate branch [alt_example/docker_swarm](https://bitbucket.org/ncatelli/cake-test/src/068466894152cb52d6545572d00fbeaaa8514962/?at=alt_example%2Fdocker_swarm)

### Introduction:
Recently I've interviewed with Cake Solutions for a DevOps engineer position. As a part of this interview, I was tasked with creating a monitored wordpress service on AWS. I've included a brief write-up on my reasonng behind my solution and the paths that I took to it.

### Requirements:
#### From Cake:
This test required that both a development and production environment be created. The focus was primarily on the devlopment environment, with the production environment being less strict. 

For development, the environment was based around docker-compose, and configured both the monitoring and wordpress stacks. In regards to wordpress, the official wordpress php-fpm container was used along with nginx as the frontend server. There were no additional mentions of database requirements in the spec so I settled on using the official mysql:5.7 container. For monitoring, prometheus acts as the primary monitoring platform with cAdvisor for exposing container specific metrics and grafana for displaying those metrics.

For Production, the primary requirement was that AWS be used and that EC2 discovery be leveraged for prometheus service discovery.

#### From Myself:
I used this test as an opportunity to experiment with terraform, a tool which I've been wanting to learn for a while. In doing this, I used terraform and docker without any additional configuration management tools making live changes.

### Tools Used for Implementation:
#### Tools Rundown:
  - Terraform
  - Packer
  - Docker Swarm
  - Docker-Compose

#### Reasoning:
I wanted to attempt to keep this deployment as close to the compose dev environment as possible and because I also wanted to limit the external dependencies, I chose to leverage a docker swarm for the production container orchestration. This limited the required software on the deployed machines to docker 1.13+. 1.13 included new features for deploying a compose stack to docker swarm that allow additional fine grained control to a production environments architecture. It also provides a secrets mechanism that I hoped to integrate into my example in a future revision. For these reason i believe chosing this version as a minimum provided me a reasonably smooth environment transition from dev to production.

For additional metrics, I chose to deploy a prometheus node_exporter which I chose to bake into an AMI along with docker. Going this route limited the application deployments that terraform had to do to only deploying the docker stack.

#### Compose files:
For the docker-compose files, I chose to use three compose files to define a base and both the dev and production environments. I started with a base docker-compose file that defined each of the services and added some simple service related labels.

```
version: '3.1'
services:
  nginx:
    image: ncatelli/nginx-cake-test:1.13.5-alpine
    labels:
      com.docker.service.name: "nginx"
    depends_on:
      - wordpress-fpm
  wordpress-fpm:
    image: wordpress:4.8.2-fpm-alpine
    labels:
      com.docker.service.name: "wordpress-fpm"
    depends_on:
      - mysql
  mysql:
    image: mysql:5.7
    labels:
      com.docker.service.name: "mysql"
  cadvisor:
    image: google/cadvisor:v0.27.1
    labels:
      com.docker.service.name: "cadvisor"
  node-exporter:
    image: prom/node-exporter:v0.14.0
    labels:
      com.docker.service.name: "node_exporter"
  prometheus:
    image: ncatelli/prometheus-cake-test:v1.7.2
    labels:
      com.docker.service.name: "prometheus"
  grafana:
    image: ncatelli/grafana-cake-test:4.5.2
    labels:
      com.docker.service.name: "grafana"
    depends_on:
      - prometheus
```

Once I had the base services defined, I defined both a ```docker-compose-dev.yml``` and ```docker-compose-prod.yml``` file for each respective environment that was concerned with pulling in the correct configurations and settings. The Dev environment is focused on setting simple dev passwords and mounting local host volumes to both the content and configurations so engineers can receive immediate feedback on their changes. An example would be 

```
  nginx:
    build:
      context: ./
      dockerfile: Dockerfile-nginx
    ports:
      - 8080:80
    volumes:
      - ./confs/nginx/nginx.conf:/etc/nginx/nginx.conf:ro
      - wordpress_docroot:/var/www/html
      - ./www/wp-content/:/var/www/html/wp-content
    links:
      - wordpress-fpm:wordpress
      - wordpress-fpm:php-fpm
      - wordpress-fpm:fpm
    networks:
      - frontend
```

In the case of the nginx service in dev, the baked in configuration from the Dockerfile-nginx image is overriden by the first defined volume along with mounting the webcontent from disk.

For production, I've added production settings as well as swarm specific deploy settings into a docker-compose-prod.yml. This is used to ultimately define a docker stack configuration file. However since docker stack does not allow multiple confguration files like docker compose I joined the two configuration files with the following command.

```bash
docker-compose -f docker-compose.yml -f docker-compose-prod.yml config > production.yml
```

This single file can be shipped to the compose managers and deployed with the following docker stack deploy command which defines a new stack called ```wordpress``` using the production conf file:

```bash
docker stack deploy --compose-file production.yml wordpress
```

#### Docker Swarm:
I've kept the swarm small for now, running a single manager and worker node. In a more ideal production environment I would be running additional manager nodes for the sake of quorum and redundancy, however for the purpose of this demonstration I thought spinning up the two different node types would be sufficient.

To keep from drifting too far out of scope for this project, I chose not to deploy any additional service-discovery tools, instead, opting to just use a small bash script to connect and pull the join tokens for each swarm node. In a longer living example, using a KV store would have been a more canonical approch to this problem.

#### Packer Builds:
To save on the complexities of configuration management at deploy time, I wanted to pre-bake as much of the turnup process as possible into an AMI. In the case of this problem, the only real external dependencies were docker 1.13+ and the prometheus node_exporter. For the purpose of revision control I chose to build this base image using packer. For future local dev, I also added a definition for a vagrant-based implementation of the AMI, though I did not utilize this heavily in my own dev on this project.

#### Terraform:
The terraform configuration was targetted directly towards AWS and handled the process of spinning up, maintaining and destroying the infrastructure around this blog as well as shipping the finalized application configuration to the docker swarm. I stuck to using the default local store and pushed all variables to this deploy with a variable file.

### Improvements:
This implementation was primarily targeted at demonstrating a deploy idea and experimenting with terraform as an IaC tool. However I've identified a few pain points that I could improve on to make this a more robust deployment.

#### Single Manager Node:
The docker swarm leveraged a single manager and worker node each respectively. In a better deployment this swarm would have at least three managers and n number of workers.

#### Single database:
MySQL wasn't configured for replication. In a more ideal configuration I would either leverage RDS or configure multiple sql servers across multiple AZ. An alternate option would be to leverage an XtraDB cluster from percona.

#### Single AZ:
Everything for this implementation was within a single AZ. To fall within the AWS SLA this application should have been deployed accross multiple AZ. To accomplish this, I would need a few more swarm nodes and to rework my database implementation to support this per the previous point.

### Summary:
I attempted to keep the scope of this implementation small to both to learn terraform as well as demostrate my approach to tackling a deployment problem. In hindsight, there are many design decisions that I would like to alter but I believe I had accomplished the goal I set for myself. I plan to continue to work on the solution to this problem to correct the issues I had addressed in the previous section.  
