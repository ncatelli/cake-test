#!/bin/bash

NEVERSION='0.14.0'
NODEEXPORTERDL="https://github.com/prometheus/node_exporter/releases/download/v$NEVERSION/node_exporter-$NEVERSION.linux-amd64.tar.gz"

sudo curl -L "$NODEEXPORTERDL" -o /tmp/node_exporter.tar.gz
sudo tar -zxf /tmp/node_exporter.tar.gz -C /tmp
sudo mv /tmp/node_exporter-$NEVERSION.linux-amd64/node_exporter /usr/local/bin/
sudo chown root:root /usr/local/bin/node_exporter

sudo cat > /tmp/node_exporter.service <<EOL
[Unit]
Description=Prometheus Node Exporter
After=network.target auditd.service

[Service]
ExecStart=/usr/local/bin/node_exporter
ExecReload=/bin/kill -HUP $MAINPID
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target
Alias=node_exporter.service
EOL

sudo mv /tmp/node_exporter.service /lib/systemd/system/node_exporter.service

sudo systemctl daemon-reload
sudo systemctl start node_exporter
sudo systemctl enable node_exporter
