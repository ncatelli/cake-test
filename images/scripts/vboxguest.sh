#!/bin/bash

mkdir /tmp/virtualbox

VERSION=$(cat /home/vagrant/.vbox_version)

sudo apt-get update
sudo apt-get install gcc dkms -y

sudo mount -o loop /home/vagrant/VBoxGuestAdditions.iso /tmp/virtualbox
sudo sh /tmp/virtualbox/VBoxLinuxAdditions.run
sudo umount /tmp/virtualbox
rmdir /tmp/virtualbox
rm /home/vagrant/*.iso
