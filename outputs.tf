output "Web Endpoint" {
  value = "${aws_elb.web.dns_name}"
}

output "Monitoring Endpoint" {
  value = "${aws_elb.monitoring.dns_name}"
}
