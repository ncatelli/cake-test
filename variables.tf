variable "public_key_path" {
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.
Ensure this keypair is added to your local SSH agent so provisioners can
connect.

Example: ~/.ssh/terraform.pub
DESCRIPTION
}

variable "private_key_path" {
}

variable "key_name" {
  description = "Desired name of AWS key pair"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "us-east-1"
}

variable "mysql_root_passwd" {
  description = "MySQL root password for webapp."
}

variable "wordpress_pass" {
  description = "wordpress password"
}

variable "prom_access_key" {}
variable "prom_secret_key" {}
variable "grafana_pass" {}
