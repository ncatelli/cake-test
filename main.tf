# Specify the provider and access details
provider "aws" {
  region = "${var.aws_region}"
}

data "aws_ami" "base_ami" {
  most_recent = true
  name_regex = "^cake-base" 
}

# Create a VPC to launch our instances into
resource "aws_vpc" "cake_test" {
  cidr_block = "10.0.0.0/16"
}

# Create an internet gateway to give our subnet access to the outside world
resource "aws_internet_gateway" "cake_test" {
  vpc_id = "${aws_vpc.cake_test.id}"
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.cake_test.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.cake_test.id}"
}

# Create a lan subnet to launch our instances into
resource "aws_subnet" "cake_test_lan" {
  vpc_id                  = "${aws_vpc.cake_test.id}"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = "${var.aws_region}a"
}

# A security group for the ELB so it is accessible via the web
resource "aws_security_group" "elb" {
  name        = "elb"
  description = "Allow inbound traffic to port 80 and 3000 at the ELB"
  vpc_id      = "${aws_vpc.cake_test.id}"

  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Grafana access from anywhere
  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Monitoring Security Group
resource "aws_security_group" "monitoring" {
  name        = "monitoring-sec-group"
  description = "Creates a security group for monitoring."
  vpc_id      = "${aws_vpc.cake_test.id}"

  # node_exporter access from anywhere
  ingress {
    from_port   = 9100
    to_port     = 9100
    protocol    = "tcp"
    cidr_blocks = ["${aws_subnet.cake_test_lan.cidr_block}"]
  }

  # node_exporter access from anywhere
  ingress {
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["${aws_subnet.cake_test_lan.cidr_block}"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Internal grafana group 
resource "aws_security_group" "grafana" {
  name        = "grafana-sec-group"
  description = "Creates a sec group for access to grafana"
  vpc_id      = "${aws_vpc.cake_test.id}"

  # node_exporter access from anywhere
  ingress {
    from_port   = 3000
    to_port     = 3000 
    protocol    = "tcp"
    cidr_blocks = ["${aws_subnet.cake_test_lan.cidr_block}"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Internal webserver group 
resource "aws_security_group" "webapp" {
  name        = "webapp-sec-group"
  description = "Creates a sec group for access to the webapp"
  vpc_id      = "${aws_vpc.cake_test.id}"

  ingress {
    from_port   = 80 
    to_port     = 80 
    protocol    = "tcp"
    cidr_blocks = ["${aws_subnet.cake_test_lan.cidr_block}"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "default" {
  name        = "default-sec-group"
  description = "Allow port 22 inbound to all nodes"
  vpc_id      = "${aws_vpc.cake_test.id}"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elb" "web" {
  name = "cake-test-elb-web"

  subnets         = ["${aws_subnet.cake_test_lan.id}"]
  security_groups = ["${aws_security_group.elb.id}"]
  instances       = ["${aws_instance.web.id}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
}

resource "aws_elb" "monitoring" {
  name = "cake-test-elb-monitoring"

  subnets         = ["${aws_subnet.cake_test_lan.id}"]
  security_groups = ["${aws_security_group.elb.id}"]
  instances       = ["${aws_instance.monitoring.id}"]

  listener {
    instance_port     = 3000
    instance_protocol = "http"
    lb_port           = 3000
    lb_protocol       = "http"
  }
}

resource "aws_key_pair" "auth" {
  key_name   = "${var.key_name}"
  public_key = "${file(var.public_key_path)}"
}

resource "aws_ebs_volume" "prometheus_metrics" {
  availability_zone = "${var.aws_region}a"
  size = 20
  tags {
    Name = "PrometheusMetrics"
  }
}

resource "aws_volume_attachment" "prometheus_metrics" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.prometheus_metrics.id}"
  instance_id = "${aws_instance.monitoring.id}"
}

resource "aws_instance" "monitoring" {
  connection {
    user = "ubuntu"
    private_key = "${file(var.private_key_path)}"
  }

  instance_type = "t2.micro"
  availability_zone= "${var.aws_region}a"

  ami = "${data.aws_ami.base_ami.id}"

  key_name = "${aws_key_pair.auth.id}"

  vpc_security_group_ids = [
    "${aws_security_group.monitoring.id}",
    "${aws_security_group.grafana.id}",
    "${aws_security_group.default.id}",
  ]

  subnet_id = "${aws_subnet.cake_test_lan.id}"

  provisioner "file" {
    source = "scripts/cadvisor.sh"
    destination = "/tmp/cadvisor.sh"
  }

  provisioner "file" {
    source = "scripts/monitoring.sh"
    destination = "/tmp/monitoring.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo bash /tmp/cadvisor.sh",
      "sudo bash /tmp/monitoring.sh ${var.prom_access_key} ${var.prom_secret_key} ${var.grafana_pass}",
    ]
  }
}

resource "aws_ebs_volume" "web_docroot_content" {
  availability_zone = "${var.aws_region}a"
  size = 20
  tags {
    Name = "WebDocrootContent"
  }
}

resource "aws_volume_attachment" "web_docroot_content" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.web_docroot_content.id}"
  instance_id = "${aws_instance.web.id}"
}

resource "aws_instance" "web" {
  connection {
    user = "ubuntu"
    private_key = "${file(var.private_key_path)}"
  }

  instance_type = "t2.micro"
  availability_zone= "${var.aws_region}a"

  ami = "${data.aws_ami.base_ami.id}"

  key_name = "${aws_key_pair.auth.id}"

  vpc_security_group_ids = [
    "${aws_security_group.webapp.id}",
    "${aws_security_group.monitoring.id}",
    "${aws_security_group.default.id}",
  ]

  subnet_id = "${aws_subnet.cake_test_lan.id}"

  provisioner "file" {
    source = "scripts/cadvisor.sh"
    destination = "/tmp/cadvisor.sh"
  }

  provisioner "file" {
    source = "scripts/webapp.sh"
    destination = "/tmp/webapp.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo bash /tmp/cadvisor.sh",
      "sudo bash /tmp/webapp.sh ${var.mysql_root_passwd} ${var.wordpress_pass}",
    ]
  }
}


