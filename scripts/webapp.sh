#!/bin/bash

if [ "$#" -ne 2 ]; then
  exit 1
fi

# create document root volume to be shared between nginx and wordpress
# containers.
docker volume create 'wordpress_docroot'

# create a frontend and backend network to link nginx to wordpress and
# wordpress to mysql.
docker network create 'frontend'
docker network create 'backend'

# Startup containers
docker run -d --name='cake_test_mysql1' --network='backend' -v /var/lib/mysql:/var/lib/mysql --label com.docker.service.name='mysql' -e MYSQL_ROOT_PASSWORD="$1" --restart=always mysql:5.7

docker create --name='cake_test_wordpress1' --network='backend' -v wordpress_docroot:/var/www/html -v /www/wp-content/:/var/www/html/wp-content --label com.docker.service.name='wordpress-fpm' --link='cake_test_mysql1:mysql' --link='cake_test_mysql1:database' --link='cake_test_mysql1:db' -e WORDPRESS_DB_PASSWORD="$2" --restart=always wordpress:4.8.2-fpm-alpine
docker network connect frontend cake_test_wordpress1
docker start cake_test_wordpress1

docker run -d --name='cake_test_nginx1' --network='frontend' -v wordpress_docroot:/var/www/html -v /www/wp-content/:/var/www/html/wp-content --label com.docker.service.name='nginx' -p 80:80 --link='cake_test_wordpress1:wordpress-fpm' --link='cake_test_wordpress1:wordpress' --link='cake_test_wordpress1:php-fpm' --link='cake_test_wordpress1:fpm' ncatelli/nginx-cake-test:1.13.5-alpine

