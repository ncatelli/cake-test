#!/bin/bash

# Start cAdvisor
docker run -d --name='cake_test_cadvisor' --privileged -v /:/rootfs:ro -v /var/run:/var/run:rw -v /sys:/sys:ro -v /var/lib/docker/:/var/lib/docker:ro --label com.docker.service.name='cadvisor' -p 8081:8080 google/cadvisor:v0.27.1

