#!/bin/bash

if [ "$#" -ne 3 ]; then
  exit 1
fi

# create a frontend and backend network to link nginx to wordpress and
# wordpress to mysql.
docker network create 'monitoring'

# Startup containers
docker run -d --name='cake_test_prometheus1' --network='monitoring' --restart=always --label com.docker.service.name='prometheus' -e AWS_ACCESS_KEY_ID="$1" -e AWS_SECRET_ACCESS_KEY="$2" ncatelli/prometheus-cake-test:v1.7.2 

docker run -d --name='cake_test_grafana1' --network='monitoring' -p 3000:3000 --label com.docker.service.name='grafana' -e GF_SECURITY_ADMIN_PASSWORD="$3" --link='cake_test_prometheus1:prometheus' ncatelli/grafana-cake-test:4.5.2

